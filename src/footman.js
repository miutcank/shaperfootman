import debug from 'debug';
import lo from 'lodash';


const _lookOver = debug('shaperfootman');

const _is = (item, type) => {
    return Object.prototype.toString.call(item).indexOf(type) > 0;
};

const _byStep = (path) => {
    return path.split('.');
};

const _clone = (dataSource) => {
    let result = JSON.parse(JSON.stringify(dataSource));
    _lookOver('Data cloning: ' + result);
    return result;
};

const _getFieldAndClear = (data, path, clear) => {
    _lookOver(
        'Get field and clear: '
        + JSON.stringify(data) +
        ', ' + JSON.stringify(path)
        + ', ' + clear);
    if (data && path && _is(path, 'Array')) {
        const key = lo.head(path);
        const rest = lo.tail(path);

        if (!Object.prototype.hasOwnProperty.call(data, key)) {
            return undefined;
        }

        if (rest && rest.length > 0) {
            return _getFieldAndClear(data[key], rest, clear);
        } else {
            let value = data[key];
            if (clear) {
                delete data[key];
            }
            _lookOver(
                'Get '
                + JSON.stringify(path)
                + ' field with value: '
                + value
            );
            return value;
        }
    } else {
        throw new TypeError(
            'data and path must be not null and path must be array'
        );
    }
};

const _deleteField = (data, pathByStep) => {
    _lookOver(
        'Delete field: '
        + JSON.stringify(data)
        + ', ' + JSON.stringify(pathByStep)
    );
    _getFieldAndClear(data, pathByStep, true);
};

const _getField = (data, pathByStep) => {
    _lookOver(
        'Get field: '
        + JSON.stringify(data)
        + ', ' + JSON.stringify(pathByStep)
    );
    return _getFieldAndClear(data, pathByStep, false);
};

const _remove = (dataSource, inputPaths) => {
    _lookOver(
        'Remove field: '
        + JSON.stringify(dataSource)
        + ', ' + JSON.stringify(inputPaths)
    );
    let dataClone = _clone(dataSource);
    if (inputPaths && _is(inputPaths, 'Array') && inputPaths.length > 0) {
        _lookOver('Remove field in progress');
        const first = lo.head(inputPaths);
        const rest = lo.tail(inputPaths);
        _deleteField(dataClone, _byStep(first));
        dataClone = _remove(dataClone, rest);
    }
    return dataClone;
};

const _createField = (dataSource, fieldList, value) => {
    const firstField = lo.head(fieldList);
    const restField = lo.tail(fieldList);
    if (restField.length > 0) {
        if (Object.prototype.hasOwnProperty.call(dataSource, firstField)) {
            if (!_is(dataSource[firstField], 'Object')) {
                throw new TypeError(
                    'Create field not possible on type: '
                    + Object.prototype.toString.call(dataSource[firstField])
                    + ' [' + firstField + ']'
                );
            }
        } else {
            dataSource[firstField] = {};
        }
        _createField(dataSource[firstField], restField, value);
    } else {
        dataSource[firstField] = value;
    }
};

const _create = (dataSource, outputRules) => {
    let dataClone = _clone(dataSource);
    const rule = lo.head(outputRules);
    const restRules = lo.tail(outputRules);
    const inputList = rule.ifields.map(
        (field) => {
            return _getField(dataClone, _byStep(field));
        }
    );
    _lookOver('Calculate value from inputs:' + inputList);
    const value = rule.how.apply(null, inputList);
    _lookOver('Calculated value:' + value);
    _createField(dataClone, _byStep(rule.ofield), value);
    if (restRules.length > 0) {
        dataClone = _create(dataClone, restRules);
    }
    return dataClone;
};

const _transform = (dataSource, outputRules) => {
    const dataWithNewFields = _create(dataSource, outputRules);
    return _remove(
        dataWithNewFields,
        outputRules.reduce(
            (aList, rule) => {
                return aList.concat(lo.difference(rule.ifields, [rule.ofield]));
            },
            []
        )
    );
};

const projection = (dataSource, configuration, ...ruleSet) => {
    _lookOver(
        'Projection: '
        + JSON.stringify(dataSource)
        + ', ' + JSON.stringify(configuration)
        + ', ' + ruleSet
    );
    if (ruleSet.length === 0) {
        return dataSource;
    } else {
        const ruleName = ruleSet.shift();
        let dataClone;
        const rule = configuration[ruleName];

        if (rule.command === 'create') {
            dataClone = _create(dataSource, rule.outputs);
        } else if (rule.command === 'transform') {
            dataClone = _transform(dataSource, rule.outputs);
        } else if (rule.command === 'remove') {
            dataClone = _remove(dataSource, rule.inputs);
        }

        return projection(dataClone, configuration, ...ruleSet);
    }
};

export default projection;
