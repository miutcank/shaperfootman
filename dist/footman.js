'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _lookOver = (0, _debug2.default)('shaperfootman');

var _is = function _is(item, type) {
    return Object.prototype.toString.call(item).indexOf(type) > 0;
};

var _byStep = function _byStep(path) {
    return path.split('.');
};

var _clone = function _clone(dataSource) {
    var result = JSON.parse(JSON.stringify(dataSource));
    _lookOver('Data cloning: ' + result);
    return result;
};

var _getFieldAndClear = function _getFieldAndClear(data, path, clear) {
    _lookOver('Get field and clear: ' + JSON.stringify(data) + ', ' + JSON.stringify(path) + ', ' + clear);
    if (data && path && _is(path, 'Array')) {
        var key = _lodash2.default.head(path);
        var rest = _lodash2.default.tail(path);

        if (!Object.prototype.hasOwnProperty.call(data, key)) {
            return undefined;
        }

        if (rest && rest.length > 0) {
            return _getFieldAndClear(data[key], rest, clear);
        } else {
            var value = data[key];
            if (clear) {
                delete data[key];
            }
            _lookOver('Get ' + JSON.stringify(path) + ' field with value: ' + value);
            return value;
        }
    } else {
        throw new TypeError('data and path must be not null and path must be array');
    }
};

var _deleteField = function _deleteField(data, pathByStep) {
    _lookOver('Delete field: ' + JSON.stringify(data) + ', ' + JSON.stringify(pathByStep));
    _getFieldAndClear(data, pathByStep, true);
};

var _getField = function _getField(data, pathByStep) {
    _lookOver('Get field: ' + JSON.stringify(data) + ', ' + JSON.stringify(pathByStep));
    return _getFieldAndClear(data, pathByStep, false);
};

var _remove = function _remove(dataSource, inputPaths) {
    _lookOver('Remove field: ' + JSON.stringify(dataSource) + ', ' + JSON.stringify(inputPaths));
    var dataClone = _clone(dataSource);
    if (inputPaths && _is(inputPaths, 'Array') && inputPaths.length > 0) {
        _lookOver('Remove field in progress');
        var first = _lodash2.default.head(inputPaths);
        var rest = _lodash2.default.tail(inputPaths);
        _deleteField(dataClone, _byStep(first));
        dataClone = _remove(dataClone, rest);
    }
    return dataClone;
};

var _createField = function _createField(dataSource, fieldList, value) {
    var firstField = _lodash2.default.head(fieldList);
    var restField = _lodash2.default.tail(fieldList);
    if (restField.length > 0) {
        if (Object.prototype.hasOwnProperty.call(dataSource, firstField)) {
            if (!_is(dataSource[firstField], 'Object')) {
                throw new TypeError('Create field not possible on type: ' + Object.prototype.toString.call(dataSource[firstField]) + ' [' + firstField + ']');
            }
        } else {
            dataSource[firstField] = {};
        }
        _createField(dataSource[firstField], restField, value);
    } else {
        dataSource[firstField] = value;
    }
};

var _create = function _create(dataSource, outputRules) {
    var dataClone = _clone(dataSource);
    var rule = _lodash2.default.head(outputRules);
    var restRules = _lodash2.default.tail(outputRules);
    var inputList = rule.ifields.map(function (field) {
        return _getField(dataClone, _byStep(field));
    });
    _lookOver('Calculate value from inputs:' + inputList);
    var value = rule.how.apply(null, inputList);
    _lookOver('Calculated value:' + value);
    _createField(dataClone, _byStep(rule.ofield), value);
    if (restRules.length > 0) {
        dataClone = _create(dataClone, restRules);
    }
    return dataClone;
};

var _transform = function _transform(dataSource, outputRules) {
    var dataWithNewFields = _create(dataSource, outputRules);
    return _remove(dataWithNewFields, outputRules.reduce(function (aList, rule) {
        return aList.concat(_lodash2.default.difference(rule.ifields, [rule.ofield]));
    }, []));
};

var projection = function projection(dataSource, configuration) {
    for (var _len = arguments.length, ruleSet = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        ruleSet[_key - 2] = arguments[_key];
    }

    _lookOver('Projection: ' + JSON.stringify(dataSource) + ', ' + JSON.stringify(configuration) + ', ' + ruleSet);
    if (ruleSet.length === 0) {
        return dataSource;
    } else {
        var ruleName = ruleSet.shift();
        var dataClone = void 0;
        var rule = configuration[ruleName];

        if (rule.command === 'create') {
            dataClone = _create(dataSource, rule.outputs);
        } else if (rule.command === 'transform') {
            dataClone = _transform(dataSource, rule.outputs);
        } else if (rule.command === 'remove') {
            dataClone = _remove(dataSource, rule.inputs);
        }

        return projection.apply(undefined, [dataClone, configuration].concat(ruleSet));
    }
};

exports.default = projection;
//# sourceMappingURL=footman.js.map