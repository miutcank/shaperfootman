import footman from '../footman';

describe('shaperfootman', function() {
    describe('delete parts', function() {
        it('SIMPLE - delete part of data', function() {
            const dataSource = {alma: {korte: 5}, tato: 'haha'};
            const config = {rule1: {command: 'remove', inputs: ['tato']}};

            const result = footman(dataSource, config, 'rule1');
            expect(result).toEqual({alma: {korte: 5}});
        });

        it('DEEP - delete deep part of data', function() {
            const dataSource = {alma: {korte: 5, pipo: 123}, tato: 'haha'};
            const config = {rule1: {command: 'remove', inputs: ['alma.pipo']}};

            const result = footman(dataSource, config, 'rule1');
            expect(result).toEqual({alma: {korte: 5}, tato: 'haha'});
        });

        it('MULTIPLE - delete two part', function() {
            const dataSource = {alma: {korte: 5, pipo: 123}, tato: 'haha'};
            const config = {
                rule1: {
                    command: 'remove',
                    inputs: ['alma.pipo', 'tato'],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result).toEqual({alma: {korte: 5}});
        });
    });

    describe('transform items', function() {
        it('FUSION - transform two item into one', function() {
            const dataSource = {
                alma: {
                    fn: 'Tirti',
                    ln: 'Mirti',
                    nn: 'Timi',
                    pipo: 123,
                },
                tato: 'haha',
            };
            const config = {
                rule1: {
                    command: 'transform',
                    outputs: [
                        {
                            ifields: [
                                'alma.fn',
                                'alma.ln',
                                'alma.nn',
                            ],
                            ofield: 'displayName',
                            how: (x, y, z) => {
                                if (z) {
                                    return z;
                                } else {
                                    return x[0] + '. ' + y;
                                }
                            },
                        },
                    ],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result)
                .toEqual(
                    {displayName: 'Timi', alma: {pipo: 123}, tato: 'haha'}
                );
        });

        it('Simple - transform', function() {
            const dataSource = {
                alma:
                    {
                        fn: 'Tirti', ln: 'Mirti', nn: 'Timi', pipo: 123,
                    },
                tato: 'haha',
            };
            const config = {
                rule1: {
                    command: 'transform',
                    outputs: [
                        {
                            ifields: ['alma.pipo'],
                            ofield: 'alma.pipo',
                            how: (x) => 3*x,
                        },
                    ],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result)
                .toEqual(
                    {
                        alma:
                            {
                                fn: 'Tirti',
                                ln: 'Mirti',
                                nn: 'Timi',
                                pipo: 369,
                            },
                        tato: 'haha',
                    }
                );
        });

        it('Default - transform', function() {
            const dataSource = {
                alma:
                    {
                        ln: 'Mirti', pipo: 123,
                    },
                tato: 'haha',
            };
            const config = {
                rule1: {
                    command: 'transform',
                    outputs: [
                        {
                            ifields: [
                                'alma.fn',
                                'alma.ln',
                                'alma.nn',
                            ],
                            ofield: 'displayName',
                            how: (x = 'Anonymous', y, z) => {
                                if (z) {
                                    return z;
                                } else {
                                    return x[0] + '. ' + y;
                                }
                            },
                        },
                    ],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result)
                .toEqual(
                    {
                        displayName: 'A. Mirti',
                        alma:
                            {
                                pipo: 123,
                            },
                        tato: 'haha',
                    }
                );
        });
    });

    describe('create parts', function() {
        it('SIMPLE - create part of data', function() {
            const dataSource = {alma: {korte: 5}, tato: 'haha'};
            const config = {
                rule1: {
                    command: 'create',
                    outputs: [
                        {
                            ifields: [
                                'alma.korte',
                            ],
                            ofield: 'lives',
                            how: (x) => 5*x,
                        },
                    ],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result).toEqual({alma: {korte: 5}, tato: 'haha', lives: 25});
        });
        it('SIMPLE - create part of data', function() {
            const dataSource = {alma: [5, 6, 7], tato: 'haha'};
            const config = {
                rule1: {
                    command: 'create',
                    outputs: [
                        {
                            ifields: [
                                'alma.2',
                            ],
                            ofield: 'lives',
                            how: (x) => 5*x,
                        },
                    ],
                },
            };

            const result = footman(dataSource, config, 'rule1');
            expect(result).toEqual({alma: [5, 6, 7], tato: 'haha', lives: 35});
        });
    });

});
